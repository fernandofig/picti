$(document).ready(function() {
	if($("#form_fotoDelete").length) atualizaBotaoDeleteFoto();
	if($("#form_carroDelete").length) atualizaBotaoDeleteCarro();
	if($("#frmLogin_username").length) $("#frmLogin_username")[0].focus();
});

function abreInclui() {
	$("#dadosIncCarro").toggle('fast');
}

function abreEditFoto(act) {
	if(act) {
		document.fotoEditData.descr.disabled = false;
		document.fotoEditData.foto_princ.disabled = false;
		document.fotoEditData.ftSubm.disabled = false;
		document.fotoEditData.ftCanc.disabled = false;
		
		document.fotoEditData.descr.value = arguments[1];
		document.fotoEditData.foto_princ.checked = arguments[2];
		document.fotoEditData.foto_princ.disabled = arguments[2];
		document.fotoEditData.fotoId.value = act;
		$("#ftEditOv").show();
		$("#ftEditDiv").show();
	} else {
		$("#ftEditDiv").hide();
		$("#ftEditOv").hide();
	}
}

function desatForm() {
	document.fotoEditData.ftSubm.disabled = true;
	document.fotoEditData.ftCanc.disabled = true;
	document.fotoEditData.foto_princ.disabled = false;
	return true;
}

function atualizaBotaoDeleteFoto() {
	if($(".chk_deleteFoto:checked").length==0)
		document.fotoDelete.deleFotoSubm.disabled = true;
	else
		document.fotoDelete.deleFotoSubm.disabled = false;
}

function atualizaBotaoDeleteCarro() {
	if($(".chk_deleteCarro:checked").length==0)
		document.carroDelete.deleCarroSubm.disabled = true;
	else
		document.carroDelete.deleCarroSubm.disabled = false;
}

function selecionaCarrosAntigos() {
	$(".chkCarroAntigo").each(function() {
		this.checked = true;
	})
}

function validaFormLogin(user, pass) {
	if(user.length<3) {
		alert("O usu�rio deve ter pelo menos 3 caracteres");
		return false;
	}
	
	if(pass.length<5) {
		alert("A senha deve ter pelo menos 5 caracteres");
		return false;
	}
	
	return true;
}

function validMudaSenha(isAdm) {
	if(isAdm)
		senhaInval = (document.frmSenha.nsenha.value.length<5 || document.frmSenha.nsenha_2.value.length<5);
	else
		senhaInval = (document.frmSenha.vsenha.value.length<5 || document.frmSenha.nsenha.value.length<5 || document.frmSenha.nsenha_2.value.length<5);
	
	if(senhaInval) {
		alert("Senhas devem ter no m�nimo 5 caracteres");
		return false;
	}
	
	if(document.frmSenha.nsenha.value.toLowerCase() != document.frmSenha.nsenha_2.value.toLowerCase()) {
		alert("As novas senhas digitadas n�o conferem. Verifique e tente novamente.");
		return false;
	}
	
	return true;
}

function validNovoUsuario() {
	if(document.frmSenha.usuario.value.length<3) {
		alert("O Nome de Usu�rio deve ter pelo menos 3 letras");
		return false;
	}
	
	if(document.frmSenha.nsenha.value.length<5 || document.frmSenha.nsenha_2.value.length<5) {
		alert("Senhas devem ter no m�nimo 5 caracteres");
		return false;
	}
	
	if(document.frmSenha.nsenha.value.toLowerCase() != document.frmSenha.nsenha_2.value.toLowerCase()) {
		alert("As novas senhas digitadas n�o conferem. Verifique e tente novamente.");
		return false;
	}
	
	return true;
}