<?
require("../inc/picti.inc");
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<html>
<head>
	<title>Atualiza��o de Banco de dados e Arquivos para o Picti</title>
</head>

<body>
<pre>
<?
$sql = " SHOW TABLES LIKE 'FOTOS' ";
$rs = mysql_query($sql);

if(mysql_num_rows($rs)==0) {
	echo "==== Fazendo upgrade de dados e arquivos... ====\n";
	
	echo "-- Criando tabela de FOTOS ... ";
	$sql = "
	CREATE TABLE FOTOS (
	  ID mediumint(9) unsigned NOT NULL auto_increment,
	  CARRO_ID mediumint(9) unsigned NOT NULL,
	  DESCRICAO varchar(255) default NULL,
	  FOTO_PRINC enum('S','N') default 'N',
	  PRIMARY KEY  (`id`),
	  UNIQUE KEY FOTO_UNIQUE (ID, CARRO_ID)
	) ENGINE=MyISAM DEFAULT CHARSET=latin1; ";
	
	mysql_query($sql);
	
	$sql = " SHOW TABLES LIKE 'FOTOS' ";
	$rs = mysql_query($sql);
	
	if(mysql_num_rows($rs)==0) die("N�o foi poss�vel criar a tabela de FOTOS! Abortando...");
	
	echo "Ok!\n";
	
	$sql = " SELECT * FROM CARRO ";
	$rs = mysql_query($sql) or die("Erro: ".mysql_error());
	
	if (mysql_num_rows($rs)==0) {
		echo "-- Importando carros / fotos atuais...\n";
	} else {
		while ($rg = mysql_fetch_assoc($rs)) {
			echo "+ Atualizando foto, ID: ".$rg["id"]." ... ";
			$sql = " INSERT INTO FOTOS (CARRO_ID, FOTO_PRINC) VALUES (".$rg["id"].", 'S') ";
			$rsFoto = mysql_query($sql);
			
			if(mysql_affected_rows()<1) {
				echo " Erro ao atualizar foto! (".mysql_error().") \n";
			} else {
				$idFoto = mysql_insert_id();
				
				if(rename($_config["docroot"] . "/images/".zeroPad($rg["id"]).".jpg",
						 $_config["docroot"] . "/images/".zeroPad($rg["id"])."-".zeroPad($idFoto).".jpg") &&
					rename($_config["docroot"] . "/images/mini/".zeroPad($rg["id"]).".jpg",
						 $_config["docroot"] . "/images/mini/".zeroPad($rg["id"])."-".zeroPad($idFoto).".jpg")) {
					echo " Ok!\n";
				} else {
					echo " Foto atualizada, mas houve erro ao atualizar o nome do arquivo da imagem!\n";
				}
			}
		}
	}
	
	echo "==== FIM! ====";
} else {
	echo "Sistema j� atualizado! Nada a fazer...";
}

?>
</pre>
</body>
</html>
<?
exit;
?>