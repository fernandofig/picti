<?
require("../inc/picti.inc");
require("auth.inc");

switch($_REQUEST["acao"]) {
case "incluir":
case "editar":
	salvaDadosCarro();
	break;
case "editFoto":
	salvaDadosFoto();
	break;
case "deletaCarro":
	deletaCarro();
	break;
case "deleteFoto":
	deletaFoto();
	break;
}

function salvaDadosCarro() {
	extract($_POST, EXTR_PREFIX_ALL, "d");
	
	$d_valor = trim($d_valor);
	if(!empty($d_valor)) {
		$d_valor = str_replace(",",".",str_replace(".","","$d_valor"));
	} else {
		$d_valor = "NULL";
	}
	
	if($_REQUEST["acao"]=="incluir") {
		$sql = " INSERT INTO CARRO (NOME, VALOR, ANO, OBS, DATA_INC)
				   VALUES ('$d_nome',$d_valor,$d_ano,'$d_obs',NOW()) ";
	} else {
		$sql = " UPDATE CARRO
				      SET NOME='$d_nome',
					       VALOR=$d_valor,
						    ANO=$d_ano,
						    OBS='$d_obs'
					 WHERE ID={$_POST['idedit']} ";
	}
	
	$rs = mysql_query($sql);
	
	if (mysql_affected_rows()<=0 && mysql_error()) die("Erro ao salvar dados... - ".mysql_error());
	
	if($_REQUEST["acao"]=="incluir") $lastcod = mysql_insert_id(); else $lastcod = $_POST['idedit'];
		
	redir("index.php?editaid=".$lastcod);
}

function salvaDadosFoto() {
	extract($_POST, EXTR_PREFIX_ALL, "d");
	
	if(!is_numeric($d_fotoId)) return false;
	
	$d_descr = trim($d_descr);
	$d_foto_princ = (@$d_foto_princ=="S"?"S":"N");
	
	if($d_foto_princ=="S") {
		$sql = " UPDATE FOTOS SET FOTO_PRINC='N' WHERE CARRO_ID=$d_carro_id ";
		$rs = mysql_query($sql);
	}
	
	$sql = " UPDATE FOTOS
			      SET DESCRICAO='$d_descr',
				       FOTO_PRINC='$d_foto_princ'
				 WHERE ID=$d_fotoId AND CARRO_ID=$d_carro_id ";
	
	$rs = mysql_query($sql);
	
	if (mysql_affected_rows()<=0 && mysql_error()) die("Erro ao salvar dados da foto... - ".mysql_error());
	
	ajustaCarroSemFotoPrinc($d_carro_id);
	
	redir("index.php?editaid=".$d_carro_id);
}

function deletaCarro() {
	global $_config;
	
	$sql = " DELETE FROM FOTOS WHERE CARRO_ID IN (".implode(",",$_REQUEST["delid"]).")";
	$rs = mysql_query($sql);
	
	$sql = " DELETE FROM CARRO WHERE ID IN (".implode(",",$_REQUEST["delid"]).") ";
	$rs = mysql_query($sql);
	
	if (mysql_affected_rows()<=0) die("Erro ao deletar carro(s)... - ".mysql_error());
	
	foreach($_REQUEST["delid"] as $cod) {
		$nomeArq = zeroPad($cod) . "-*.jpg";
		
		foreach(glob($_config["docroot"] . DIRECTORY_SEPARATOR . "images" . DIRECTORY_SEPARATOR . $nomeArq) as $imgfile) @unlink($imgfile);
		foreach(glob($_config["docroot"] . DIRECTORY_SEPARATOR . "images" . DIRECTORY_SEPARATOR . "mini" . DIRECTORY_SEPARATOR . $nomeArq) as $imgfile) @unlink($imgfile);
	}
	
	redir("index.php");
}

function deletaFoto() {
	global $_config;
	
	$sql = " DELETE FROM FOTOS WHERE CARRO_ID = ".$_REQUEST["carro_id"]." AND ID IN (".implode(",",$_REQUEST["deleFt"]).")";
	$rs = mysql_query($sql);
	
	if (mysql_affected_rows()<=0) die("Erro ao deletar foto(s)... - ".mysql_error());
	
	foreach($_REQUEST["deleFt"] as $umId) {
		$nomeArq = zeroPad($_REQUEST["carro_id"]) . "-" . zeroPad($umId) . ".jpg";
		
		@unlink($_config["docroot"] . DIRECTORY_SEPARATOR . "images" . DIRECTORY_SEPARATOR . $nomeArq);
		@unlink($_config["docroot"] . DIRECTORY_SEPARATOR . "images" . DIRECTORY_SEPARATOR . "mini" . DIRECTORY_SEPARATOR . $nomeArq);
	}
	
	ajustaCarroSemFotoPrinc($_REQUEST["carro_id"]);
	
	redir("index.php?editaid=".$_REQUEST["carro_id"]);
}

function ajustaCarroSemFotoPrinc($carroId) {
	//No caso da foto principal ter sido deletada ou desabilitada, setamos a foto principal
	//como a �ltima inserida.
	if(!carroTemFoto($carroId, true)) {
		$sql = "SELECT MAX(ID) AS ULTIMO_ID FROM FOTOS WHERE CARRO_ID = ".$carroId;
		$rs = mysql_query($sql);
		
		if (mysql_num_rows($rs)>0 && $rg = mysql_fetch_assoc($rs)) {
			$sql = " UPDATE FOTOS SET FOTO_PRINC = 'S' WHERE CARRO_ID = ".$carroId." AND ID = ".$rg["ULTIMO_ID"];
			mysql_query($sql);
			
			if (mysql_affected_rows()<=0) die("Erro ao setar foto principal... - ".mysql_error());
		}
	}
}
?>