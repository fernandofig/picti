<?
require("../inc/picti.inc");
require("auth.inc");

if(!$logState[0]) redir("index.php");
if($logState[0] && $logState[1]=="admin" && $_SESSION["senha"] == sha1("admin")) $nova_instal = true;

$msg = "";

if(!empty($_POST) || !empty($_REQUEST["deluser"])) {
	foreach($_POST as $qs => $val) $_POST[$qs] = strtolower(trim($val));
	
	if($nova_instal) {
		if(!valid_OldSenha($_POST["vsenha"])) {
			$msg = "A senha antiga est� incorreta.";
		} elseif(!valid_NovaSenha($_POST["nsenha"], $_POST["nsenha_2"])) {
			$msg = "As novas senhas digitadas n�o conferem, ou n�o<br>puderam ser validadas (m�nimo 5 caracteres)";
		} elseif(strtolower(trim($_POST["nsenha"]))=="admin") {
			$msg = "A nova senha n�o pode ser igual � senha padr�o.";
		} else {
			$baseUsuarios[$logState[1]] = sha1(strtolower(trim($_POST["nsenha"])));
			$_SESSION["senha"] = sha1(strtolower(trim($_POST["nsenha"])));
			salvaBaseUsuarios($baseUsuarios);
			redir("usuarios.php");
		}
	} elseif($logState[1]=="admin") {
		if(!empty($_POST["editPassAdmin"]) && array_key_exists($_POST["usuario"], $baseUsuarios)) {
			if(!valid_NovaSenha($_POST["nsenha"], $_POST["nsenha_2"])) {
				$msg = "As novas senhas digitadas n�o conferem, ou n�o<br>puderam ser validadas (m�nimo 5 caracteres)";
			} else {
				$baseUsuarios[$_POST["usuario"]] = sha1(strtolower(trim($_POST["nsenha"])));
				salvaBaseUsuarios($baseUsuarios);
				redir("usuarios.php");
			}
		} elseif(!empty($_REQUEST["deluser"]) && array_key_exists($_REQUEST["deluser"], $baseUsuarios)) {
			$usuarioDel = array($_REQUEST["deluser"] => $baseUsuarios[$_REQUEST["deluser"]]);
			$baseUsuarios = array_diff_key($baseUsuarios, $usuarioDel);
			salvaBaseUsuarios($baseUsuarios);
			redir("usuarios.php");
		} else {
			if(!valid_Usuario($_POST["usuario"])) {
				$msg = "O usu�rio especificado j� existe ou n�o<br>p�de ser validado (m�nimo 3 caracteres)";
			} elseif(!valid_NovaSenha($_POST["nsenha"], $_POST["nsenha_2"])) {
				$msg = "As novas senhas digitadas n�o conferem, ou n�o<br>puderam ser validadas (m�nimo 5 caracteres)";
			} else {
				$baseUsuarios[$_POST["usuario"]] = sha1(strtolower(trim($_POST["nsenha"])));
				salvaBaseUsuarios($baseUsuarios);
				redir("usuarios.php");
			}
		}
	} else {
		if(!valid_OldSenha($_POST["vsenha"])) {
			$msg = "A senha antiga est� incorreta.";
		} elseif(!valid_NovaSenha($_POST["nsenha"], $_POST["nsenha_2"])) {
			$msg = "As novas senhas digitadas n�o conferem, ou n�o<br>puderam ser validadas (m�nimo 5 caracteres)";
		} else {
			$baseUsuarios[$logState[1]] = sha1(strtolower(trim($_POST["nsenha"])));
			$_SESSION["senha"] = sha1(strtolower(trim($_POST["nsenha"])));
			$msg = '<span style="color:#00ffff;">Senha modificada com sucesso!</span>';
			salvaBaseUsuarios($baseUsuarios);
		}
	}
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
	<meta http-equiv=pragma content="no-cache">
	<meta http-equiv=expires content="Mon, 06 Jan 1990 00:00:01 GMT">
	<title>Cadastro de Usu�rios</title>
	<link rel="stylesheet" type="text/css" href="adm.css">
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.2.6/jquery.min.js" language="JavaScript"></script>
	<script src="adm.js" language="JavaScript"></script>
</head>
<body>
<div id="admUser"><a href="/adm/index.php">Adm. Carros</a> | <a href="/adm/index.php?logout=1">Logout</a></div>
<table cellspacing="0" border="0" width="100%">
	<tr>
		<td align="center">
			<table class="tabadm">
				<tr>
					<td align="center">
<?
if($nova_instal) {
?>
<p>Esta � uma nova instala��o do sistema. A �rea administrativa est� usando<br>
a senha padr�o, e � requerido agora que voc� a altere, por quest�o de<br>
seguran�a. A nova senha n�o pode ser igual � senha padr�o do sistema.<br></p>
<?
	formSenha($logState[1], true);
} elseif($logState[1]=="admin") {
	if(isset($baseUsuarios[$_REQUEST["editpass"]])) {
?>
<h3>Altera��o de senha de acesso</h3>
<p>Repita duas vezes a nova senha abaixo:</p>
<?
		formSenha($_REQUEST["editpass"], true, true);
	} else {
?>
<p><b><a href="javascript:void(0);" onclick="$('#formNovoUsuario').toggle('fast');return false;">NOVO USU�RIO</a></b>
<div id="formNovoUsuario"<?=(!empty($_POST["usuario"]) && !empty($msg)?' style="display:block;"':'')?>>
<? formSenha($_POST["usuario"], false); ?>
</div></p>
<?
		$baseUsuariosNoAdmin = array_diff_key($baseUsuarios, array("admin" => $baseUsuarios["admin"]));
		
		if(empty($baseUsuariosNoAdmin)) {
?>
<i>N�o h� usu�rios cadastrados.</i>
<?
		} else {
?>
	<table cellpadding="3" cellspacing="0" border="0">
<?
			foreach($baseUsuariosNoAdmin as $usu => $passUser) {
?>
		<tr>
			<td><b><?=$usu?></b></td>
			<td style="font-size:8pt;"><a href="usuarios.php?editpass=<?=$usu?>">Mudar senha</a> | <a href="usuarios.php?deluser=<?=$usu?>" onclick="return confirm('A exclus�o deste usu�rio � permanente!\nConfirma?');">Excluir</a></td>
		</tr>
<?
			}
?>
	</table>
<?
		}
	}
} else {
?>
<h3>Altera��o de senha de acesso</h3>
<p>Preencha abaixo sua senha antiga e repita duas vezes sua nova senha.</p>
<?
	formSenha($logState[1], true);
}
?>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
</body>
</html>
<?
function formSenha($user = null, $passChange = false, $isAdmin = false) {
	global $msg;
?>
<table cellpadding="3" cellspacing="0" border="0">
	<form name="frmSenha" action="usuarios.php" method="post" onsubmit="return <?=($passChange?'validMudaSenha('.($isAdmin ? 'true':'false').')':'validNovoUsuario()')?>;">
<?
	if($passChange) {
?>
	<tr>
		<td>Usu�rio:</td>
		<td><b><?=$user?></b><input type="Hidden" name="usuario" value="<?=$user?>"></td>
	</tr>
<?
		if(!$isAdmin) {
?>
	<tr>
		<td style="border-bottom:1px solid silver;padding-bottom:10px;">Senha Antiga:</td>
		<td style="border-bottom:1px solid silver;padding-bottom:10px;"><input type="Password" name="vsenha" value=""></td>
	</tr>
<?
		} else {
?>
	<input type="Hidden" name="editPassAdmin" value="1">
<?
		}
?>
	<tr>
		<td style="padding-top:10px;">Nova senha:</td>
		<td style="padding-top:10px;"><input type="Password" name="nsenha" value=""></td>
	</tr>
	<tr>
		<td>Repita a nova senha:</td>
		<td><input type="Password" name="nsenha_2" value=""></td>
	</tr>
<?
	} else {
?>
	<tr>
		<td>Usu�rio:</td>
		<td><input type="Text" name="usuario" value="<?=$user?>"></td>
	</tr>
	<tr>
		<td>Senha:</td>
		<td><input type="Password" name="nsenha" value=""></td>
	</tr>
	<tr>
		<td>Repita a senha:</td>
		<td><input type="Password" name="nsenha_2" value=""></td>
	</tr>
<?
	}
	
	if(!empty($msg)) {
?>
	<tr>
		<td align="center" colspan="2" style="color:#ffcccc;font-weight:bold;"><?=$msg?></td>
	</tr>
<?
	}
?>
	<tr>
		<td colspan="2" align="center" style="padding-top:10px;"><input type="Submit" value="<?=($passChange?"Mudar a senha":"Salvar usu�rio")?>"></td>
	</tr>
	</form>
</table>
<?
}

function valid_Usuario($usuario) {
	global $baseUsuarios;
	
	return (strlen($usuario)>=3 && !array_key_exists($usuario, $baseUsuarios));
}

function valid_OldSenha($osenha) {
	global $baseUsuarios, $logState;
	
	return (strlen($osenha)>=5 && $baseUsuarios[$logState[1]] == sha1($osenha));
}

function valid_NovaSenha($nsenha1, $nsenha2) {
	global $logState;
	
	return (strlen($nsenha1)>=5 && strlen($nsenha2)>=5 && strtolower($nsenha1)==strtolower($nsenha2));
}
?>