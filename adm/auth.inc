<?
function abreBaseUsuarios() {
	global $_config;
	
	$base = @file_get_contents($_config["siteroot"].DIRECTORY_SEPARATOR."users.pw");
	
	if(!$base) {
		$userbase = array("admin" => sha1("admin"));
		salvaBaseUsuarios($userbase);
	} elseif(!($userbase = unserialize($base))) {
		if(!is_array($userbase)) {
			$userbase = array("admin" => sha1("admin"));
			salvaBaseUsuarios($userbase);
		}
	}
	
	return $userbase;
}

function salvaBaseUsuarios($base) {
	global $_config;
	
	$ub = fopen($_config["siteroot"].DIRECTORY_SEPARATOR."users.pw", "w+");
	fwrite($ub, serialize($base));
	fclose($ub);
}

function isLogado() {
	global $baseUsuarios;
	
	if(@$_SESSION["usuario"] != "" && @$_SESSION["senha"] != "") {
		if(!array_key_exists($_SESSION["usuario"], $baseUsuarios)) {
			unset($_SESSION["usuario"]);
			unset($_SESSION["senha"]);
			return array(false, "Usu�rio n�o existe");
		}
		
		if($baseUsuarios[$_SESSION["usuario"]] != $_SESSION["senha"]) {
			unset($_SESSION["usuario"]);
			unset($_SESSION["senha"]);
			return array(false, "Senha incorreta");
		}
	} else {
		return array(false, "");
	}
	
	return array(true, $_SESSION["usuario"]);
}

function formLogin() {
	global $logState;
?>
<table cellpadding="5" border="0">
	<form action="index.php" method="post" name="frmLogin" onsubmit="return validaFormLogin(document.frmLogin.user.value, document.frmLogin.pass.value);">
	<input type="Hidden" name="acao" value="login">
	<tr>
		<td colspan="2" align="center"><span style="font-size:12pt; font-weight:bold;">Autentique-se</span></td>
	</tr>
	<tr>
		<td colspan="2" align="center">
		� necess�rio autenticar-se<br>
		para acessar esta �rea.
		</td>
	</tr>
	<tr>
		<td>Usu�rio:</td>
		<td><input type="Text" name="user" id="frmLogin_username" value="" size="22"></td>
	</tr>
	<tr>
		<td>Senha:</td>
		<td><input type="Password" name="pass" value="" size="15"> <input type="Submit" value="Ok" style="width:38px;"></td>
	</tr>
<?
			if(!$logState[0] && !empty($logState[1])) {
?>
	<tr>
		<td colspan="2" align="center" style="color:#ffcccc;font-weight:bold;"><?=$logState[1]?></td>
	</tr>	
<?
			}
?>
	</form>
</table>
<?
}

session_start();

if($_REQUEST["logout"]) {
	$_SESSION = array();
	
	if (isset($_COOKIE[session_name()])) setcookie(session_name(), '', time()-42000, '/');
	
	session_destroy();
	
	redir("index.php");
}

$baseUsuarios = abreBaseUsuarios();

if(!($_SERVER["PHP_SELF"]=="/adm/index.php" && $_POST["acao"]=="login")) {
	$logState = isLogado();
	
	if($logState[0] && $_SERVER["PHP_SELF"]!="/adm/usuarios.php" && $_SESSION["senha"]==sha1("admin")) redir("usuarios.php");
	if(!$logState[0] && ($_SERVER["PHP_SELF"]!="/adm/index.php" || !empty($_GET) || !empty($_POST))) redir("index.php");
}
?>