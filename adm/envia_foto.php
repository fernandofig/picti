<?
require("../inc/picti.inc");
require("auth.inc");

$arquivo = &$_FILES["arqfoto"];
$tammax = $_config["tammax"] * 1024 * 1024;

if ($arquivo["error"]==2 || $arquivo["size"]>$tammax) die ("Tamanho do arquivo muito grande (M�x de " . ($tammax/1024)/1024 . " Mb.)");
if ($arquivo["type"]!="image/jpeg" and $arquivo["type"]!="image/pjpeg") die ("Tipo de arquivo inv�lido! (S�o permitidos somente arquivos JPEG)");

switch ($arquivo["error"]) {
	case 1:
		die ("Arquivo ultrapassa o limite de tamanho permitido pelo servidor.");
		break;
	case 3:
		die ("Arquivo n�o foi totalmente transferido! Tente novamente!");
		break;
	case 4:
		die ("Nenhum arquivo selecionado!");
		break;
}

$aq_tmp = $_config["docroot"] . "/adm/upload/" . basename($arquivo["tmp_name"]) . ".jpg";

if (move_uploaded_file($arquivo["tmp_name"], $aq_tmp)) {
	
	if(!get_magic_quotes_gpc()) $_REQUEST["desc"] = addslashes($_REQUEST["desc"]);
	
	$sql = " INSERT INTO FOTOS (CARRO_ID, DESCRICAO, FOTO_PRINC) VALUES (".$_REQUEST["carro_id"].", '".$_REQUEST["desc"]."', 'N')";
	$rs = mysql_query($sql);
	if (mysql_affected_rows()<=0) die("Erro ao incluir foto para o carro... - ".mysql_error());
	
	$lastFotoCod = mysql_insert_id();
	
	$img = @imagecreatefromjpeg($aq_tmp);
	
	if ($img) {
		list($wid,$hgt) = array(imagesx($img),imagesy($img));
		list($nwid,$nhgt) = getPropImgSize(640,480, $wid,$hgt);
		list($twid,$thgt) = getPropImgSize(120,90, $wid,$hgt);
		$imgr = @imagecreatetruecolor($nwid, $nhgt);
		$imgt = @imagecreatetruecolor($twid, $thgt);
		imagecopyresampled($imgr,$img,0,0,0,0,$nwid,$nhgt,$wid,$hgt);
		imagecopyresampled($imgt,$imgr,0,0,0,0,$twid,$thgt,$nwid,$nhgt);
		imagegammacorrect($imgr,1,1.30);
		imagegammacorrect($imgt,1,1.30);
		imageinterlace($imgr,1);
		imageinterlace($imgt,1);
		
		$foto     = $_config["docroot"] . "/images/" . zeroPad($_REQUEST["carro_id"]) . "-" . zeroPad($lastFotoCod) . ".jpg";
		$fotoMini = $_config["docroot"] . "/images/mini/" . zeroPad($_REQUEST["carro_id"]) . "-" . zeroPad($lastFotoCod) . ".jpg";
		
		imagejpeg($imgr, $foto,75);
		imagejpeg($imgt, $fotoMini,75);
		
		if($_REQUEST["foto_princ"]=="S") setaFotoPrincipal($_REQUEST["carro_id"], $lastFotoCod);
	} else {
		$sql = " DELETE FROM FOTOS WHERE ID = " . $lastFotoCod . " AND CARRO_ID = " . $_REQUEST["carro_id"];
		$rs = mysql_query($sql);
		die("Erro ao tratar/salvar imagem");
	}
	
	unlink($aq_tmp);
	
	redir("index.php?editaid=" . $_REQUEST["carro_id"]);
} else {
	print "Erro ao transferir arquivo. Informa��es de debug...\n";
	print_r($_FILES);
}
?>