<?
function zeroPad($num, $padsize = 6) {
	return str_pad($num, $padsize,"0",STR_PAD_LEFT);
}

function redir($urlStr) {
	if(strpos(strtolower($urlStr),"http://")===false) {
		header("Location: http://" . $_SERVER["HTTP_HOST"] . dirname($_SERVER["PHP_SELF"]) . "/" . $urlStr);
	} else {
		header("Location: $urlStr");
	}
	
	exit;
}

function getPropImgSize($dwidth,$dheight,$owidth,$oheight) {
	$fwid = (int) $dwidth;
	$fhgt = (int) $dheight;
	
	if ($dwidth && $dheight && $owidth && $oheight) {
		if ((($dwidth>$dheight) && ($owidth>$oheight)) || (($dwidth>$dheight) && ($owidth<$oheight))) {
			$fhgt = (int) (($dwidth / $owidth) * $oheight);
		} elseif ((($dwidth<$dheight) && ($owidth<$oheight)) || (($dwidth<$dheight) && ($owidth>$oheight))) {
			$fwid = (int) (($dheight / $oheight) * $owidth);
		}
	}
	return array($fwid,$fhgt);
}

function setaFotoPrincipal($carro_id, $foto_id) {
	$sql = " UPDATE FOTOS SET FOTO_PRINC = 'N' WHERE CARRO_ID = " . $carro_id ;
	$rs = mysql_query($sql);
	
	$sql = " UPDATE FOTOS SET FOTO_PRINC = 'S' WHERE CARRO_ID = " . $carro_id . " AND ID = " . $foto_id;
	$rs = mysql_query($sql);
	
	if(mysql_affected_rows()<=0) return false;
	
	return true;
}

function carroTemFoto($carro_id, $sohFotoPrinc = false) {
	$sql = " SELECT COUNT(ID) FROM FOTOS WHERE CARRO_ID = ".$carro_id;
	if($sohFotoPrinc) $sql .= " AND FOTO_PRINC = 'S' ";
	
	$rs = mysql_query($sql);
	if($rg = mysql_fetch_row($rs)) return ($rg[0]>0?true:false); else return false;
}

if (!function_exists('array_diff_key')) {
	function array_diff_key() {
		$argCount   = func_num_args();
		$argValues  = func_get_args();
		$valuesDiff = array();
		
		if ($argCount < 2) return false;
		
		foreach ($argValues as $argParam) if(!is_array($argParam)) return false;
		
		foreach ($argValues[0] as $valueKey => $valueData)	{
			for ($i = 1; $i < $argCount; $i++) if(isset($argValues[$i][$valueKey])) continue 2;
			
			$valuesDiff[$valueKey] = $valueData;
		}
		
		return $valuesDiff;
	}
}

function array_shift_noix(&$array){
	reset($array);
	$key = key($array);
	$removed = $array[$key];
	unset($array[$key]);
	return $removed;
}
?>