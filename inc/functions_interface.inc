<?
/*
INTERFACE DO USU�RIO
*/

function cospeCarros($pag, $limit = 16, $carrosPorLin = 4, $mostraPreco = true, $mostraPaginacao = false, $mostraAleatorio = false) {
	
	if(!is_numeric($pag)) $pag = 1; else $pag = (integer)$pag;
	
	$imgCount = 0;
	
	$sql = " SELECT
					CARRO.ID,
					CARRO.NOME,
					CARRO.VALOR,
					CARRO.ANO,
					CARRO.DATA_INC,
					FOTOS.ID AS ID_FOTO_PRINC
				FROM CARRO
				LEFT JOIN FOTOS ON (CARRO.ID = FOTOS.CARRO_ID AND FOTOS.FOTO_PRINC = 'S')
				";
	if($mostraAleatorio) {
		$sql .= " ORDER BY RAND() ";
	} else {
		$sql .= " ORDER BY CARRO.DATA_INC DESC, CARRO.ID DESC ";
	}
	
	$oset = ($pag-1) * $_config["showimgs"];
	$prevPg = ($pag>1?$pag-1:false);
	
	$sql = $sql . " LIMIT $oset, 999999 ";//. showimgs;
	
	$rs = mysql_query($sql) or die("Erro: ".mysql_error());
	
	if (mysql_num_rows($rs)==0) {
		echo '<div style="font-family:Verdana;font-size:10pt;margin:20px;">Nenhum carro dispon�vel no banco de dados<br>de estoque. Tente novamente mais tarde.<br></div>';
	} else {
?>
	<div align="center">
	<table width="5" border="0" cellspacing="1" cellpadding="10">
<?
		while ($rg = mysql_fetch_assoc($rs)) {
			
			if($imgCount % $carrosPorLin == 0) {
?>
		<tr valign="top">
<?
			}
			
			$linkedit = "/estoque/zoom.php?id={$rg['ID']}";
?>
			<td width="25%" align="center">
				<table cellspacing="0" cellpadding="0" border="0">
					<tr align="center"> 
						<td><a href="<?=$linkedit?>"><img src="/images/mini/<?=($rg["ID_FOTO_PRINC"] ? zeroPad($rg["ID"])."-".zeroPad($rg["ID_FOTO_PRINC"]).".jpg":"naodisp.png") ?>" title="<?=$rg["NOME"] . " (" . $rg["ANO"] . ")"?>" border="1"></a></td>
					</tr>
					<tr> 
						<td>
						<div class="thumbnailtitle" align="center"> 
						<?=$rg["NOME"]?>
						<?=(empty($rg['VALOR']) || !$mostraPreco?'':'<br><span class="precoestoque">R$ '.number_format($rg['VALOR'], 2, ",", ".").'</span>')?>
						</div>
						</td>
					</tr>
				</table>
			</td>
<?
			$imgCount++;
			
			if($imgCount % $carrosPorLin == 0) {
?>
		</tr>
<?
			}
			
			if($imgCount==$limit) break;
		}
		
		if($rg = mysql_fetch_assoc($rs)) {
			$nextPg = $pag + 1;
		} else {
			$nextPg = false;
		}
		
		if($mostraPaginacao) {
?>
			<tr>
				<td colspan="<?=$carrosPorLin?>" align="center" class="pageText" nowrap>
				<? if($prevPg!==false) { ?>
				<a href="estoque.php?pag=<?=$prevPg?>">&laquo;&nbsp;P�gina&nbsp;Anterior</a>
				<? } ?>
				<? if($prevPg!==false && $nextPg!==false) { ?>&nbsp;&nbsp;&nbsp;&nbsp;<? } ?>
				<? if($nextPg!==false) { ?>
				<a href="estoque.php?pag=<?=$nextPg?>">Pr�xima&nbsp;P�gina&nbsp;&raquo;</a>
				<? } ?>
				</td>
			</tr>
<?
		}
?>
		</table>
		</div>
<?
	}
}

function pegaIdNavCarro($id, $posNav) {
	$id = addslashes($id);
	
	if($posNav == "prox") {
		$cmp = "<";
		$ord = "DESC";
	} elseif($posNav == "ant") {
		$cmp = ">";
		$ord = "";
	}
	
	$sql = " SELECT ID
				FROM CARRO
				WHERE ID <> $id
				AND   DATA_INC $cmp (SELECT DATA_INC FROM CARRO WHERE ID = $id)
				ORDER BY DATA_INC $ord, ID $ord
				LIMIT 1 ";
	
	$rs = mysql_query($sql);
	if(mysql_num_rows($rs)==0) {
		return null;
	} else {
		if($rg = mysql_fetch_row($rs)) return $rg[0]; else return null;
	}
}

function pegaIdNavFoto($id, $idFoto, $posNav) {
	$id = addslashes($id);
	
	if($posNav == "prox") {
		$cmp = ">";
		$ord = "";
	} elseif($posNav == "ant") {
		$cmp = "<";
		$ord = "DESC";
	}
	
	$sql = " SELECT FOTOS.ID
				FROM FOTOS
				WHERE FOTOS.ID $cmp $idFoto
				AND   FOTOS.CARRO_ID = $id
				ORDER BY FOTOS.ID $ord
				LIMIT 1 ";
	
	$rs = mysql_query($sql);
	if(mysql_num_rows($rs)==0) {
		return null;
	} else {
		if($rg = mysql_fetch_row($rs)) return $rg[0]; else return null;
	}
}

function getPrimIdFotoCarro($id) {
	$sql = " SELECT MIN(FOTOS.ID) AS PRIMID FROM FOTOS WHERE CARRO_ID = $id ";
	$rs = mysql_query($sql);
	if($rg = mysql_fetch_row($rs)) return $rg[0];
}

function pegaDadosZoom($id, $idFoto = null) {
	global $_config;
	
	if(!empty($id)) {
		if(is_numeric($id)) {
			$sql = " SELECT ID, NOME, VALOR, ANO, OBS FROM CARRO WHERE ID = $id ";
			$rs = mysql_query($sql);
			
			if (mysql_num_rows($rs)==0 || !($rg = mysql_fetch_assoc($rs))) {
				redir("estoque.php");
			} else {
				$rg["OBS"] = trim($rg["OBS"]);
				
				if(empty($idFoto) || !is_numeric($idFoto)) $idFoto = getPrimIdFotoCarro($id);
				
				if(is_null($idFoto))
					return array(
						"NOME_CARRO"     => trim($rg["NOME"]),
						"VALOR_CARRO"    => (empty($rg["VALOR"])?'<span style="color:#cccccc;">Consulte-nos!</span>':number_format($rg["VALOR"], 2, ",", ".")),
						"ANO_CARRO"      => $rg["ANO"],
						"OBS_CARRO"      => $rg["OBS"],
						"DESC_FOTO"      => (empty($rgFoto["DESCRICAO"])?'':' ('.$rgFoto["DESCRICAO"].')'),
						"IMG_PATH"       => "/images/naodisp.png",
						"IMG_W"          => 640,
						"IMG_H"          => 480,
						"LIST_MINIS"     => "",
						"LNK_CARRO_ANT"  => (is_null($lnk_carro_ant)?'<img src="arlesimages/in_tsign_solid_black_previous_car.gif" align="absmiddle" border="0" height="30" width="73">':'<a href="zoom.php?id='.$lnk_carro_ant.'"><img src="arlesimages/tsign_solid_black_previous_car.gif" align="absmiddle" border="0" height="30" width="73" alt="[&lt; OFERTA ANTERIOR]" title="OFERTA ANTERIOR"></a>'),
						"LNK_CARRO_PROX" => (is_null($lnk_carro_prox)?'<img src="arlesimages/in_tsign_solid_black_next_car.gif" align="absmiddle" border="0" height="30" width="73">':'<a href="zoom.php?id='.$lnk_carro_prox.'"><img src="arlesimages/tsign_solid_black_next_car.gif" align="absmiddle" border="0" height="30" width="73" alt="[PR�XIMA OFERTA &gt;]" title="PR�XIMA OFERTA"></a>'),
						"LNK_FOTO_ANT"   => "&nbsp;",
						"LNK_FOTO_PROX"  => "&nbsp;",
						"LNK_ESTOQUE"    => '<a href="estoque.php"><img src="arlesimages/tsign_solid_black_index.gif" border="0" height="30" width="30" alt="ESTOQUE" title="RETORNAR AO CAT�LOGO DE ESTOQUE"></a>'
					);
				
				$sql = " SELECT ID, DESCRICAO FROM FOTOS WHERE ID = $idFoto ";
				$rsFoto = mysql_query($sql);
				
				if(mysql_num_rows($rsFoto)>0 && $rgFoto = mysql_fetch_assoc($rsFoto)) {
					$rgFoto["DESCRICAO"] = trim($rgFoto["DESCRICAO"]);
					
					$nomeImg = zeroPad($id)."-".zeroPad($idFoto).".jpg";
					
					$lnk_carro_ant  = pegaIdNavCarro($id, "ant");
					$lnk_carro_prox = pegaIdNavCarro($id, "prox");
					$lnk_foto_ant   = pegaIdNavFoto($id, $idFoto, "ant");
					$lnk_foto_prox  = pegaIdNavFoto($id, $idFoto, "prox");
					
					//var_dump(get_defined_vars());
					
					if(file_exists($_config["docroot"] . "/images/" . $nomeImg)) {
						$img = @imagecreatefromjpeg($_config["docroot"] . "/images/" . $nomeImg);
						list($img_width,$img_height) = array(imagesx($img),imagesy($img));
						
						return array(
							"NOME_CARRO"     => trim($rg["NOME"]),
							"VALOR_CARRO"    => (empty($rg["VALOR"])?'<span style="color:#cccccc;">Consulte-nos!</span>':number_format($rg["VALOR"], 2, ",", ".")),
							"ANO_CARRO"      => $rg["ANO"],
							"OBS_CARRO"      => $rg["OBS"],
							"DESC_FOTO"      => (empty($rgFoto["DESCRICAO"])?'':' ('.$rgFoto["DESCRICAO"].')'),
							"IMG_PATH"       => "/images/" . $nomeImg,
							"IMG_W"          => $img_width,
							"IMG_H"          => $img_height,
							"LIST_MINIS"     => listaFotosZoom($id, $idFoto),
							"LNK_CARRO_ANT"  => (is_null($lnk_carro_ant)?'<img src="arlesimages/in_tsign_solid_black_previous_car.gif" align="absmiddle" border="0" height="30" width="73">':'<a href="zoom.php?id='.$lnk_carro_ant.'"><img src="arlesimages/tsign_solid_black_previous_car.gif" align="absmiddle" border="0" height="30" width="73" alt="[&lt; OFERTA ANTERIOR]" title="OFERTA ANTERIOR"></a>'),
							"LNK_CARRO_PROX" => (is_null($lnk_carro_prox)?'<img src="arlesimages/in_tsign_solid_black_next_car.gif" align="absmiddle" border="0" height="30" width="73">':'<a href="zoom.php?id='.$lnk_carro_prox.'"><img src="arlesimages/tsign_solid_black_next_car.gif" align="absmiddle" border="0" height="30" width="73" alt="[PR�XIMA OFERTA &gt;]" title="PR�XIMA OFERTA"></a>'),
							"LNK_FOTO_ANT"   => (is_null($lnk_foto_ant)?'<img src="arlesimages/in_tsign_solid_black_previous.gif" align="absmiddle" border="0" height="30" width="30">':'<a href="zoom.php?id='.$id.'&idFoto='.$lnk_foto_ant.'"><img src="arlesimages/tsign_solid_black_previous.gif" align="absmiddle" border="0" height="30" width="30" alt="[&lt; FOTO ANTERIOR]" title="FOTO ANTERIOR"></a>'),
							"LNK_FOTO_PROX"  => (is_null($lnk_foto_prox)?'<img src="arlesimages/in_tsign_solid_black_next.gif" align="absmiddle" border="0" height="30" width="30">':'<a href="zoom.php?id='.$id.'&idFoto='.$lnk_foto_prox.'"><img src="arlesimages/tsign_solid_black_next.gif" align="absmiddle" border="0" height="30" width="30" alt="[PR�XIMA FOTO &gt;]" title="PR�XIMA FOTO"></a>'),
							"LNK_ESTOQUE"    => '<a href="estoque.php"><img src="arlesimages/tsign_solid_black_index.gif" border="0" height="30" width="30" alt="ESTOQUE" title="RETORNAR AO CAT�LOGO DE ESTOQUE"></a>'
						);
					} else {
						return array(
							"NOME_CARRO"     => trim($rg["NOME"]),
							"VALOR_CARRO"    => (empty($rg["VALOR"])?'<span style="color:#cccccc;">Consulte-nos!</span>':number_format($rg["VALOR"], 2, ",", ".")),
							"ANO_CARRO"      => $rg["ANO"],
							"OBS_CARRO"      => $rg["OBS"],
							"DESC_FOTO"      => (empty($rgFoto["DESCRICAO"])?'':' ('.$rgFoto["DESCRICAO"].')'),
							"IMG_PATH"       => "/images/naodisp.png",
							"IMG_W"          => 640,
							"IMG_H"          => 480,
							"LIST_MINIS"     => listaFotosZoom($id, $idFoto),
							"LNK_CARRO_ANT"  => (is_null($lnk_carro_ant)?'<img src="arlesimages/in_tsign_solid_black_previous_car.gif" align="absmiddle" border="0" height="30" width="73">':'<a href="zoom.php?id='.$lnk_carro_ant.'"><img src="arlesimages/tsign_solid_black_previous_car.gif" align="absmiddle" border="0" height="30" width="73" alt="[&lt; OFERTA ANTERIOR]" title="OFERTA ANTERIOR"></a>'),
							"LNK_CARRO_PROX" => (is_null($lnk_carro_prox)?'<img src="arlesimages/in_tsign_solid_black_next_car.gif" align="absmiddle" border="0" height="30" width="73">':'<a href="zoom.php?id='.$lnk_carro_prox.'"><img src="arlesimages/tsign_solid_black_next_car.gif" align="absmiddle" border="0" height="30" width="73" alt="[PR�XIMA OFERTA &gt;]" title="PR�XIMA OFERTA"></a>'),
							"LNK_FOTO_ANT"   => (is_null($lnk_foto_ant)?'<img src="arlesimages/in_tsign_solid_black_previous.gif" align="absmiddle" border="0" height="30" width="30">':'<a href="zoom.php?id='.$id.'&idFoto='.$lnk_foto_ant.'"><img src="arlesimages/tsign_solid_black_previous.gif" align="absmiddle" border="0" height="30" width="30" alt="[&lt; FOTO ANTERIOR]" title="FOTO ANTERIOR"></a>'),
							"LNK_FOTO_PROX"  => (is_null($lnk_foto_prox)?'<img src="arlesimages/in_tsign_solid_black_next.gif" align="absmiddle" border="0" height="30" width="30">':'<a href="zoom.php?id='.$id.'&idFoto='.$lnk_foto_prox.'"><img src="arlesimages/tsign_solid_black_next.gif" align="absmiddle" border="0" height="30" width="30" alt="[PR�XIMA FOTO &gt;]" title="PR�XIMA FOTO"></a>'),
							"LNK_ESTOQUE"    => '<a href="estoque.php"><img src="arlesimages/tsign_solid_black_index.gif" border="0" height="30" width="30" alt="ESTOQUE" title="RETORNAR AO CAT�LOGO DE ESTOQUE"></a>'
						);
					}
				} else {
					return array(
						"NOME_CARRO"     => trim($rg["NOME"]),
						"VALOR_CARRO"    => (empty($rg["VALOR"])?'<span style="color:#cccccc;">Consulte-nos!</span>':number_format($rg["VALOR"], 2, ",", ".")),
						"ANO_CARRO"      => $rg["ANO"],
						"OBS_CARRO"      => $rg["OBS"],
						"DESC_FOTO"      => (empty($rgFoto["DESCRICAO"])?'':' ('.$rgFoto["DESCRICAO"].')'),
						"IMG_PATH"       => "/images/naodisp.png",
						"IMG_W"          => 640,
						"IMG_H"          => 480,
						"LIST_MINIS"     => "",
						"LNK_CARRO_ANT"  => (is_null($lnk_carro_ant)?'<img src="arlesimages/in_tsign_solid_black_previous_car.gif" align="absmiddle" border="0" height="30" width="73">':'<a href="zoom.php?id='.$lnk_carro_ant.'"><img src="arlesimages/tsign_solid_black_previous_car.gif" align="absmiddle" border="0" height="30" width="73" alt="[&lt; OFERTA ANTERIOR]" title="OFERTA ANTERIOR"></a>'),
						"LNK_CARRO_PROX" => (is_null($lnk_carro_prox)?'<img src="arlesimages/in_tsign_solid_black_next_car.gif" align="absmiddle" border="0" height="30" width="73">':'<a href="zoom.php?id='.$lnk_carro_prox.'"><img src="arlesimages/tsign_solid_black_next_car.gif" align="absmiddle" border="0" height="30" width="73" alt="[PR�XIMA OFERTA &gt;]" title="PR�XIMA OFERTA"></a>'),
						"LNK_FOTO_ANT"   => "&nbsp;",
						"LNK_FOTO_PROX"  => "&nbsp;",
						"LNK_ESTOQUE"    => '<a href="estoque.php"><img src="arlesimages/tsign_solid_black_index.gif" border="0" height="30" width="30" alt="ESTOQUE" title="RETORNAR AO CAT�LOGO DE ESTOQUE"></a>'
					);
				}
			}
		} else {
			redir("estoque.php");
		}
	} else {
		redir("estoque.php");
	}
}

/*
INTERFACE ADM
*/
function pgRaizAdm() {
?>
<table width="100%" class="tabadm">
	<tr>
		<td align="center">
			<b><a href="#" onclick="abreInclui();">INCLUIR CARRO</a></b>
			<div id="dadosIncCarro">
			<? formEditDadosCarro("incluir"); ?>
			</div>
		</td>
	</tr>
</table>
<br>
<table width="500" class="tabadm">
	<tr>
		<td align="center" style="border-bottom: 1px solid silver;padding-bottom:3px;"><b>CARROS CADASTRADOS</b></td>
	</tr>
	<? listaCarros(); ?> 
</table>
<?
}

function pgEdicaoCarro() {
?>
<table class="tabadm">
	<tr>
		<td align="center" style="border-bottom: 1px solid silver;padding-bottom:3px;">
			<div style="position:absolute;"><a href="index.php" style="font-size:8pt;">&laquo; Voltar</a></div>
			<b>DADOS DO CARRO</b>
			<? formEditDadosCarro("editar", $_REQUEST["editaid"]); ?>
		</td>
	</tr>
	<tr>
		<td align="center" style="border-bottom: 1px solid silver;padding-bottom:3px;">
			<b>UPLOAD DE FOTO DO CARRO</b>
			<table border="0">
			<form action="envia_foto.php" method="post" enctype="multipart/form-data">
			<input type="Hidden" name="carro_id" value="<?=$_REQUEST["editaid"]?>">
			<input type="Hidden" name="MAX_FILE_SIZE" value="<?=($_config["tammax"] * 1024 * 1024)?>">
			<tr>
				<td>Foto:</td><td><input name="arqfoto" type="File" size="30"></td>
			</tr>
			<tr>
				<td>Texto descritivo:</td><td><input name="desc" type="Text" value="" style="width:100%;"></td>
			</tr>
<?
			if(carroTemFoto($_REQUEST["editaid"])) {
?>
			<tr>
				<td colspan="2" align="center"><label for="ftprc"><input id="ftprc" type="Checkbox" name="foto_princ" value="S">Foto principal <span style="font-size:8pt;">(ser� exibida na lista geral de estoque)</span></label></td>
			</tr>
<?
			} else {
?>
			<input type="Hidden" name="foto_princ" value="S">
<?
			}
?>
			<tr>
				<td colspan="2" align="center"><input type="Submit" value="Enviar Foto"></td>
			</tr>
			</form>
			</table>
		</td>
	</tr>
	<tr>
		<td align="center">
		<b>FOTOS DO CARRO CADASTRADAS</b><br>
		<? listaFotosCarro($_REQUEST["editaid"]) ?>
		</td>
	</tr>
</table>
<?
}

function formEditDadosCarro($acao, $id = null) {
	if($acao == "editar") {
		$sql = " SELECT * FROM CARRO WHERE ID=$id";
		$rs = mysql_query($sql);
		if (mysql_num_rows($rs)==0) {
			die("Registro inexistente!");
		} else {
			if($rg=mysql_fetch_assoc($rs)) {
				array_change_key_case($rg, CASE_UPPER);
				extract($rg);
			} else {
				die("Erro ao obter dados do banco de dados!");
			}
		}
	} else {
		$NOME  = "";
		$ANO   = "";
		$VALOR = "";
		$OBS   = "";
	}
?>
<table border="0">
	<form action="salva.php" method="post">
	<input type="Hidden" name="acao" value="<?=$acao?>">
	<tr>
		<td nowrap>NOME DO CARRO:</td>
		<td><input type="Text" name="nome" value="<?=$NOME?>" maxlength="128" style="width:280px;"></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td align="right">ANO:<input type="Text" name="ano" value="<?=$ANO?>" size="5" maxlength="4">&nbsp;&nbsp;VALOR (R$): <input type="Text" name="valor" value="<?=str_replace(".",",",$VALOR)?>" size="10" maxlength="12"></td>
	</tr>
	<tr>
		<td valign="top">OBSERVA��ES:</td>
		<td align="right"><textarea name="obs" rows="5" style="width:280px;font-size:9pt;" wrap="soft"><?=$OBS?></textarea></td>
	</tr>
	<tr>
		<td colspan="2" align="right"><? if($acao=="editar") { ?><input type="Hidden" name="idedit" value="<?=$id?>"><input type="Button" name="volta" value="<< Voltar" onclick="javascript:location.href='index.php';"> <input type="Button" name="delete" value="Apagar este carro e fotos" onclick="javascript:if(confirm('As Fotos do carro tamb�m ser�o exclu�das, e esta opera��o n�o pode\nser revertida. Deseja mesmo continuar?')) location.href='salva.php?acao=deletaCarro&delid[]=<?=$id?>';"> <? } ?><input type="Submit" value="<?=($acao!="editar" ? "Incluir" : "Salvar")?>"></td>
	</tr>
	</form>
</table>
<?
}

function listaCarros() {
?>
<form action="salva.php" id="form_carroDelete" name="carroDelete" method="post">
<tr>
	<td style="padding-top:3px;" align="center">
	<?
	$temvelho = false;
	
	$sql = " SELECT
					CARRO.ID,
					CARRO.NOME,
					CARRO.VALOR,
					CARRO.ANO,
					IF(DATA_INC<=DATE_SUB(CURDATE(), INTERVAL 2 MONTH),1,0) AS EHVELHO,
					FOTOS.ID AS ID_FOTO_PRINC
					FROM CARRO
					LEFT JOIN FOTOS ON (CARRO.ID = FOTOS.CARRO_ID AND FOTOS.FOTO_PRINC = 'S')
					ORDER BY DATA_INC, ID";
	$rs = mysql_query($sql);
	if (mysql_num_rows($rs)==0) {
		echo "(Nenhum carro cadastrado)<br>";
	} else {
		while ($rg = mysql_fetch_assoc($rs)) {
			$linkedit = "index.php?editaid={$rg['ID']}";
?>
		<div class="imgTb">
			<table cellpadding="0" cellspacing="0" border="0">
				<input type="Hidden" name="acao" value="deletaCarro">
				<tr>
					<td colspan="3" align="center" style="font-weight:bold;font-size:8pt;font-family:Arial;"><?=($rg["EHVELHO"] ? "<font color=\"yellow\">" . $rg["NOME"] . "</font>" : $rg["NOME"])?></td>
				</tr>
				<tr>
					<td colspan="3"><a href="<?=$linkedit?>"><img src="/images/mini/<?=($rg["ID_FOTO_PRINC"] ? zeroPad($rg["ID"])."-".zeroPad($rg["ID_FOTO_PRINC"]).".jpg":"naodisp.png") ?>" title="<?=$rg["NOME"] . " (" . $rg["ANO"] . ")"?> - Clique para editar" style="border:2px <?= ($rg["EHVELHO"]?'solid yellow':'solid #303030') ?>;"></a></td>
				</tr>
				<tr>
					<td width="1"><input type="Checkbox" onclick="atualizaBotaoDeleteCarro();" name="delid[]" id="chk_<?=$rg["ID"]?>" value="<?=$rg["ID"]?>" class="<?=($rg["EHVELHO"] ? 'chk_deleteCarro chkCarroAntigo' : 'chk_deleteCarro')?>"></td>
					<td style="font-family:Arial;font-size:8pt;"><label for="chk_<?=$rg["ID"]?>" style="cursor:pointer;">Deletar</label></td>
					<td style="font-family:Arial;font-size:8pt;" align="right"><a href="<?=$linkedit?>">Editar</a></td>
				</tr>
			</table>
		</div>
<?
			if($rg["EHVELHO"]) $temvelho = true;
		}
		mysql_free_result($rs);
?>
	</td>
</tr>
<tr>
	<td align="center">
		<table cellpadding="2" cellspacing="0" border="0">
			<tr>
				<? if($temvelho) { ?>
				<td align="center">
				<a href="javascript:void(0);" onclick="selecionaCarrosAntigos(); this.blur(); return false;">
				Selecionar carros antigos<br>
				<span style="font-size:8pt;">(mais de 2 meses)</span>
				</a>
				<? } ?>
				</td>
				<td><input type="Submit" name="deleCarroSubm" value="Deletar carros selecionados"></td>
				<td align="center">
				<a href="javascript:void(0);" onclick="document.carroDelete.reset(); atualizaBotaoDeleteCarro(); this.blur(); return false;">
				Limpar sele��es
				</a>
			</tr>
		</table>
	</td>
</tr>
<?
	}

	if($temvelho) {
?>
<tr>
	<td style="color:#ffcccc;font-size:8pt;font-weight:bold;font-family:Arial;" align="center">
	Existem Carros que est�o cadastrados h� mais de 2 meses. Eles est�o destacados em amarelo, clique no link<br>
	"Selecionar Carros Antigos" acima para selecionar todos esses carros, e clique em "Deletar" para confirmar.
	</td>
</tr>
<?
	}
?>
</form>
<?
}

function listaFotosZoom($id, $idFoto = null) {
	
	$sql = " SELECT COUNT(ID) AS TEMFOTOS FROM FOTOS WHERE CARRO_ID = ". $id;
	$rs = mysql_query($sql);
	
	if(($rg = mysql_fetch_row($rs)) && (integer)$rg[0]<2) return "";
	
	if(empty($idFoto)) {
		$sql = " SELECT MIN(ID) AS PRIM_ID FROM FOTOS WHERE CARRO_ID = ". $id;
		$rs = mysql_query($sql);
		
		$rg = mysql_fetch_row($rs);
		
		$idFoto = (integer)$rg[0];
	}
	
	$sql = " SELECT * FROM FOTOS WHERE CARRO_ID = " .$id. " ORDER BY ID ";
	$rs = mysql_query($sql);
	
	$minis = array();
	$cnt = 0;
	
	while($rg = mysql_fetch_assoc($rs)) $minis[] = array((integer)$rg["ID"], ((integer)$rg["ID"]==$idFoto ? true : false), $rg["DESCRICAO"]);
	
	foreach($minis as $umMini) {
		if($umMini[1]) break;
		
		$cnt++;
	}
	
	$edOffset = (count(array_slice($minis, $cnt+1))>=2 ? 2 : 5 - count(array_slice($minis, $cnt+1)) - 1);
	
	$offset = (count($minis)>5 ? ($cnt - 2 < 0 ? 0 : $cnt - $edOffset) : 0);
	
	$minis = array_slice($minis, $offset, 5);
	
	$fotos = "";
	
	foreach($minis as $umMini) {
		$fotos .= '<td align="center">';
		$fotos .= '<a href="zoom.php?id='.$id.'&idFoto='.$umMini[0].'">';
		$fotos .= '<img src="/images/mini/'.zeroPad($id).'-'.zeroPad($umMini[0]).'.jpg" height="80" style="border:5px solid '.($umMini[1] ? 'white' : '#333333').'" title="'.$umMini[2].'">';
		$fotos .= '</a></td>';
	}
	
	$fotos = '<table width="100%" border="0" cellpadding="0" cellspacing="0" style="margin:4px;"><tr>'.$fotos.'</tr></table>';
	
	return $fotos;
}

function listaFotosCarro($id) {
	$sql = " SELECT * FROM FOTOS WHERE CARRO_ID = " .$id. " ORDER BY ID ";
	$rs = mysql_query($sql);
	
	if (mysql_num_rows($rs)==0) {
		echo '<div align="center" style="font-style:italic;padding:15px;">N�o h� nenhuma foto dispon�vel para este Carro.</div>';
	} else {
?>
	<div id="ftEditDiv">
		<table align="center" border="0" cellpadding="3" cellspacing="0" style="margin-top:15px;margin-left:4px;margin-right:4px;">
			<form name="fotoEditData" action="salva.php" method="post" onsubmit="return desatForm();">
			<input type="Hidden" name="acao" value="editFoto">
			<input type="Hidden" name="fotoId" value="">
			<input type="Hidden" name="carro_id" value="<?=$_REQUEST["editaid"]?>">
			<tr>
				<td width="10%" nowrap>Descri��o:</td>
				<td><input name="descr" type="Text" value="" style="width:100%;"></td>
			</tr>
			<tr>
				<td colspan="2" align="center"><label for="ftprcEd"><input id="ftprcEd" type="Checkbox" name="foto_princ" value="S">Foto principal <span style="font-size:8pt;">(ser� exibida na lista geral de estoque)</span></label></td>
			</tr>
			<tr>
				<td colspan="2" align="center"><input type="Submit" name="ftSubm" value="Salvar"> <input name="ftCanc" type="Button" value="Cancelar" onclick="abreEditFoto(0);"></td>
			</tr>
			</form>
		</table>
	</div>
	<table width="100%" cellpadding="0" cellspacing="0" border="0">
		<form action="salva.php" name="fotoDelete" id="form_fotoDelete" method="post" onsubmit="return confirm('As fotos selecionadas ser�o permanentemente exclu�das.\nConfirma?');">
		<input type="Hidden" name="acao" value="deleteFoto">
		<input type="Hidden" name="carro_id" value="<?=$_REQUEST["editaid"]?>">
		<tr>
			<td>
<?
		while ($rg = mysql_fetch_assoc($rs)) {
?>
				<div class="imgTb">
					<table cellpadding="1" cellspacing="0" border="0">
						<tr>
							<td colspan="3"><img src="/images/mini/<?=zeroPad($rg["CARRO_ID"])?>-<?=zeroPad($rg["ID"])?>.jpg" title="<?=$rg["DESCRICAO"]?>" style="border:3px <?= ($rg["FOTO_PRINC"]=="S"?'dotted white':'solid #303030') ?>;"></td>
						</tr>
						<tr>
							<td width="1"><input type="Checkbox" onclick="atualizaBotaoDeleteFoto();" class="chk_deleteFoto" id="chk_<?=$rg["ID"]?>" name="deleFt[]" value="<?=$rg["ID"]?>"></td>
							<td class="tdItemAdm"><label for="chk_<?=$rg["ID"]?>" style="cursor:pointer;">Deletar</label></td>
							<td class="tdItemAdm" align="right"><a href="javascript:void(0);" onclick="abreEditFoto(<?= $rg["ID"] ?>,'<?= $rg["DESCRICAO"] ?>',<?= ($rg["FOTO_PRINC"]=="S"?'true':'false') ?>); return false;">Ed. Dados</a></td>
						</tr>
					</table>
				</div>
<?
		}
		
		mysql_free_result($rs);
		
?>
			</td>
		</tr>
		<tr>
			<td align="center"><input type="Submit" name="deleFotoSubm" value="Deletar fotos selecionadas"></td>
		</tr>
		</form>
	</table>
<?
	}
}
?>